﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject circleiPhone,circleiPad,circleiPhoneX;    
    bool stopCoroutines;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnEnable()
    {
        stopCoroutines = false;
        StartCoroutine(BallHint());
    }
    public void StopActiveCoroutines()
    {
        stopCoroutines = true;
    }
    IEnumerator BallHint()
    {
        for(int i = 0; i < 1; i++)
        {
            yield return new WaitForSeconds(1);
        }
        while (true && !stopCoroutines)
        {
            GameObject _circle = Instantiate(circleiPad,gameObject.transform);
            _circle.transform.position = Camera.main.WorldToScreenPoint(GameObject.FindGameObjectWithTag("MainBall").transform.position);
            /*float ratio = (float)Screen.currentResolution.width / Screen.currentResolution.height;
            //4:3 Ratio
            if (ratio > 0.74f)
            {
                _circle = Instantiate(circleiPad, gameObject.transform);
            }
            //18:9 Ratio
            else if(ratio < 0.47f)
            {
                _circle = Instantiate(circleiPhoneX, gameObject.transform);
            }
            //16:9 Ratio
            else
            {
                _circle = Instantiate(circleiPhone, gameObject.transform);
            }*/

            StartCoroutine(Zoomer(_circle));
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator Zoomer(GameObject @object)
    {
        for(int i = 0; i < 20; i++)
        {
            @object.GetComponent<RectTransform>().localScale += new Vector3(0.1f,0.1f,0);
            @object.GetComponent<Image>().color = new Color(@object.GetComponent<Image>().color.r,@object.GetComponent<Image>().color.g,
            @object.GetComponent<Image>().color.b, @object.GetComponent<Image>().color.a-0.05f);
            yield return new WaitForSeconds(0.05f);
        }
        Destroy(@object);
    }
}
