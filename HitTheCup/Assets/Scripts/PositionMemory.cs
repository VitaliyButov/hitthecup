﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PositionMemory : ScriptableObject
{
    List <ObjectInfo> objects;

    public void Remember() {
                objects = new List<ObjectInfo>();
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("MovableObject");
            foreach (GameObject g in gameObjects)
            {
                ObjectInfo objectInfo = new ObjectInfo();
                objectInfo.position = g.transform.position;
                objectInfo.rotation = g.transform.rotation;
                objectInfo.name = g.name;
                objects.Add(objectInfo);
            }
    }    
    public void RestoreAllObjectsOnScene()
    {
            foreach(ObjectInfo objectInfo in objects)
            {
                GameObject g = GameObject.Find(objectInfo.name);
                g.transform.position = objectInfo.position;
                g.transform.rotation = objectInfo.rotation;
            }
    }


}
[System.Serializable]
public class ObjectInfo
{
    public Vector3 position;
    public Quaternion rotation;
    public string name;
}
