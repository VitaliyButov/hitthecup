﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelIcon : MonoBehaviour
{
    Slider slider;
    public LevelSystem ls;
    public Image level1, level2, level3, level4, level5;
    public Sprite levelOpen, levelLocked;
    public TextMeshProUGUI level1txt, level2txt, level3txt, level4txt, level5txt;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        if (ls.CurrentLevelIndex == 1|| ls.CurrentLevelIndex == 6 || ls.CurrentLevelIndex == 11)
        {
            slider.value = 0;
            level1.sprite = levelOpen;
            level2.sprite = levelLocked;
            level3.sprite = levelLocked;
            level4.sprite = levelLocked;
            level5.sprite = levelLocked;
        }
        else if (ls.CurrentLevelIndex == 2 || ls.CurrentLevelIndex == 7 || ls.CurrentLevelIndex == 12)
        {
            slider.value = 0.25f;
            level1.sprite = levelOpen;
            level2.sprite = levelOpen;
            level3.sprite = levelLocked;
            level4.sprite = levelLocked;
            level5.sprite = levelLocked;
        }
        else if (ls.CurrentLevelIndex == 3 || ls.CurrentLevelIndex == 8 || ls.CurrentLevelIndex == 13)
        {
            slider.value = 0.5f;
            level1.sprite = levelOpen;
            level2.sprite = levelOpen;
            level3.sprite = levelOpen;
            level4.sprite = levelLocked;
            level5.sprite = levelLocked;
        }
        else if (ls.CurrentLevelIndex == 4 || ls.CurrentLevelIndex == 9 || ls.CurrentLevelIndex == 14)
        {
            slider.value = 0.75f;
            level1.sprite = levelOpen;
            level2.sprite = levelOpen;
            level3.sprite = levelOpen;
            level4.sprite = levelOpen;
            level5.sprite = levelLocked;
        }
        else if (ls.CurrentLevelIndex == 5 || ls.CurrentLevelIndex == 10 || ls.CurrentLevelIndex == 15)
        {
            slider.value = 1;
            level1.sprite = levelOpen;
            level2.sprite = levelOpen;
            level3.sprite = levelOpen;
            level4.sprite = levelOpen;
            level5.sprite = levelOpen;        
        }

        int fifthLevelRange = (int)ls.CurrentLevelIndex / 5;
        if ((int)ls.CurrentLevelIndex % 5 >= 1)
        {
            level1txt.text = (1 + 5 * fifthLevelRange).ToString();
            level2txt.text = (2 + 5 * fifthLevelRange).ToString();
            level3txt.text = (3 + 5 * fifthLevelRange).ToString();
            level4txt.text = (4 + 5 * fifthLevelRange).ToString();
            level5txt.text = (5 + 5 * fifthLevelRange).ToString();
        }
        else
        {
            level1txt.text = (1 + 5 * (fifthLevelRange-1)).ToString();
            level2txt.text = (2 + 5 * (fifthLevelRange-1)).ToString();
            level3txt.text = (3 + 5 * (fifthLevelRange-1)).ToString();
            level4txt.text = (4 + 5 * (fifthLevelRange-1)).ToString();
            level5txt.text = (5 + 5 * (fifthLevelRange-1)).ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
