﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject trajectoryBall,arrow,WinMenu, confetti;
    UIManager uiManager;
    Vector3 startPosition;
    CameraController cameraController;
    LineRenderer lineRenderer;
    Vector3[] positions;
    bool mouseListener,winMenuCreated;
    // Start is called before the first frame update
    void Start()
    {
        mouseListener = true;
        lineRenderer = GetComponent<LineRenderer>();
        positions = new Vector3[90];
        gameObject.GetComponent<SphereCollider>().sharedMaterial.bounceCombine = PhysicMaterialCombine.Maximum;
        gameObject.GetComponent<SphereCollider>().sharedMaterial.bounciness = 0.98f;
        cameraController = Camera.main.transform.GetComponent<CameraController>();
        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        startPosition = gameObject.transform.position;
        lineRenderer.enabled = true;
        winMenuCreated = false;
        CalculateTrajectory();
    }

    // Update is called once per frame
    void Update()
    {
        if (mouseListener)
        {
            if (gameObject.GetComponent<Rigidbody>().isKinematic)
            {
                gameObject.transform.Rotate(0, 1, 1);
            }
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        gameObject.GetComponent<Rigidbody>().isKinematic = false;
                        cameraController.FollowBall = true;
                        lineRenderer.enabled = false;
                        mouseListener = false;
                        arrow.SetActive(false);
                        uiManager.StopActiveCoroutines();
                    }
                }
            }
        }
    }

    public void CalculateTrajectory()
    {
        GameObject _trajectoryBall = Instantiate(trajectoryBall,transform.position,Quaternion.identity);
        Physics.autoSimulation = false;

        for(int i =0; i < 180; i++)
        {
            Physics.Simulate(Time.fixedDeltaTime);
            if(i%2==0)positions[i/2] = _trajectoryBall.transform.position;
        }

        Physics.autoSimulation = true;
        Destroy(_trajectoryBall);
        lineRenderer.SetPositions(positions);
    }
    public void ResetPosition()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.transform.position = startPosition;
        gameObject.GetComponent<SphereCollider>().sharedMaterial.bounceCombine = PhysicMaterialCombine.Maximum;
        gameObject.GetComponent<SphereCollider>().sharedMaterial.bounciness = 0.98f;
        cameraController.FollowBall = false;
        lineRenderer.enabled = true;
        mouseListener = true;
        arrow.SetActive(true);
        uiManager.OnEnable();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Cup"|| collision.gameObject.tag == "CupParent")
        {

            gameObject.GetComponent<SphereCollider>().sharedMaterial.bounceCombine = PhysicMaterialCombine.Minimum;
            gameObject.GetComponent<SphereCollider>().sharedMaterial.bounciness = 0.6f;
        }
        if(collision.gameObject.tag == "Cup")
        {
            if (!winMenuCreated)
            {
                Vibration.Vibrate(100);
                Instantiate(WinMenu, GameObject.Find("Canvas").transform);
                Instantiate(confetti);
                GameObject.Find("");
                winMenuCreated = true;
            }
        }
    }
}
