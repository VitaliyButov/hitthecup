﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitSoundStarter : MonoBehaviour
{
    AudioSource audioSource;
    AudioClip standart;
    public AudioClip changePot;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        standart = audioSource.clip;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "MainBall")
        {
            audioSource.clip = standart;
            audioSource.Play();
        }
        else if(collision.gameObject.tag == "MovableObject")
        {
            audioSource.clip = changePot;
            audioSource.Play();
        }
    }
}
