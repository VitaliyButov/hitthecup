﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject ball;
    Vector3 desiredPosition, startPosition;
    private bool followBall;

    public bool FollowBall
    {
        get => followBall;
        set => followBall = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.localPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (followBall)
        {
            desiredPosition = new Vector3(ball.transform.position.x,ball.transform.position.y+6.2f,ball.transform.localPosition.z-10.27f);
            transform.localPosition = Vector3.Lerp(transform.localPosition,desiredPosition,0.1f);
        }
        else if(transform.position!= startPosition)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, startPosition, 0.05f);
        }
    }
}
