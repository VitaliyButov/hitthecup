﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public LevelSystem ls;
    public PositionMemory pm;
    public bool editMode;
    // Start is called before the first frame update
    void Start()
    {
        TinySauce.OnGameStarted("Current level is: "+ls.CurrentLevelIndex.ToString());
        if (!editMode)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(ls.CurrentLevelIndex - 1))
            {

            }
            else
            {
                SceneManager.LoadScene(ls.CurrentLevelIndex - 1);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Remember()
    {
        pm.Remember();
    }
    public void RestoreAllObjectsOnScene()
    {
        pm.RestoreAllObjectsOnScene();
    }
}
