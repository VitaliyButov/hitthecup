﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teacher : MonoBehaviour
{
    bool showLesson;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(183, 113);
        showLesson = true;
        StartCoroutine(Mover());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            showLesson = false;
        }
    }

    IEnumerator Mover()
    {
        while (showLesson)
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(183, 113);
            gameObject.GetComponent<Image>().enabled = true;
            gameObject.GetComponent<Image>().color = new Color(gameObject.GetComponent<Image>().color.r,gameObject.GetComponent<Image>().color.g,gameObject.GetComponent<Image>().color.b,1); ;
            for (int i = 0; i < 60; i++)
            {
                gameObject.GetComponent<RectTransform>().position = gameObject.GetComponent<RectTransform>().position + new Vector3(-3.15f, -3.5f, 0);
                gameObject.GetComponent<Image>().color = new Color(gameObject.GetComponent<Image>().color.r, gameObject.GetComponent<Image>().color.g, gameObject.GetComponent<Image>().color.b,gameObject.GetComponent<Image>().color.a-0.016f);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
