﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class LevelSystem : ScriptableObject
{
    private int _currentLevelIndex;
    public int CurrentLevelIndex
    {
        get { return _currentLevelIndex; }
        set
        {
            _currentLevelIndex = value;
            SaveData();
        }
    }


    public void LoadNextLevel()
    {
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level Unlocked");
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level Unlocked");
        TinySauce.OnGameFinished("Level "+ _currentLevelIndex.ToString()+" is finished",1);
        _currentLevelIndex++;
        _currentLevelIndex = Mathf.Clamp(_currentLevelIndex, 1, 15);
        SceneManager.LoadScene(_currentLevelIndex-1);
        SaveData();        
    }


    protected void OnEnable()
    {
        LoadData();
    }


    public void SaveData()
    {
        GameData gd = new GameData();
        gd.currentLevelIndex = _currentLevelIndex;
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector ss = new SurrogateSelector();
        string path = Application.persistentDataPath + "/GameData.json";
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, gd);
        stream.Close();
    }
    public void LoadData()
    {
        string path = Application.persistentDataPath + "/GameData.json";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector ss = new SurrogateSelector();
            FileStream stream = new FileStream(path, FileMode.Open);
            GameData gd = formatter.Deserialize(stream) as GameData;
            _currentLevelIndex = gd.currentLevelIndex;
            stream.Close();
            
        }
        else
        {
            Debug.Log("Save file not found");
            _currentLevelIndex = 1;
            SaveData();
        }
    }
}
[System.Serializable]
public class GameData
{
    public int currentLevelIndex;

}
