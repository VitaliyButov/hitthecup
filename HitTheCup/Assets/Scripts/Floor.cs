﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    AudioSource audioSource;
    int countOfCollisions;
    BallController ballController;
    DateTime time;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        countOfCollisions = 0;
        ballController = GameObject.FindGameObjectWithTag("MainBall").GetComponent<BallController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (countOfCollisions >= 1 && (DateTime.Now - time).Seconds >= 2)
        {
            countOfCollisions = 0;
            ballController.ResetPosition();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "MainBall")
        {
            time = DateTime.Now;
            audioSource.Play();
            countOfCollisions++;
            if (countOfCollisions >= 2)
            {
                ballController.ResetPosition();
                countOfCollisions = 0;
            }
        }

    }
}
