﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnEnable()
    {
        StartCoroutine(Move());
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator Move()
    {
        float f = 0;
        while (true)
        {
            gameObject.transform.Translate(new Vector3(0,Mathf.Sin(f)*0.1f,0));
            f += 0.1f;
            yield return null;
        }
    }

}
