﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : MonoBehaviour
{
    //GameObject capturedObject;
    BallController ballController;
    Vector3 mousePos,hitedObjPosition;
    public float sensitivity;
    bool hitted,needToCalculate,moving;
    private Vector3 objPosition, transformedVectorPosition;

    public Vector3 ObjPosition { get => objPosition; set => objPosition = value; }
    public Vector3 TransformedVectorPosition { get => transformedVectorPosition; set => transformedVectorPosition = value; }

    // Start is called before the first frame update
    void Start()
    {
        ObjPosition = transform.position;
        TransformedVectorPosition = ObjPosition;
        ballController = GameObject.FindGameObjectWithTag("MainBall").GetComponent<BallController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.gameObject == gameObject)
                {
                    //capturedObject = hit.collider.gameObject.transform.parent.gameObject;
                    mousePos = Input.mousePosition;
                    hitted = true;
                }
            }
        }
        if (Input.GetMouseButton(0) && hitted)
        {
            Vector3 moveResult = (mousePos-Input.mousePosition)*sensitivity;
            TransformedVectorPosition = transform.position + new Vector3(-moveResult.x,0,-moveResult.y);
            mousePos = Input.mousePosition;
            transform.position = Vector3.Lerp(transform.position, TransformedVectorPosition, 0.1f);
            ballController.CalculateTrajectory();
        }
        if (Input.GetMouseButtonUp(0) && hitted)
        {
            TransformedVectorPosition = ObjPosition;
            hitted = false;
            needToCalculate = true;
        }
        
        if (transform.position == TransformedVectorPosition && !hitted && needToCalculate)
        {
            
            //transform.parent.position = Vector3.Lerp(transform.parent.position, transformedVectorPosition, 1f);
            ballController.CalculateTrajectory();
            needToCalculate = false;
        }
        else if (transform.position != TransformedVectorPosition && !hitted)
        {
            TransformedVectorPosition = new Vector3(TransformedVectorPosition.x,transform.position.y,TransformedVectorPosition.z);
            if(!GameObject.Find("LevelManager").GetComponent<LevelManager>().editMode)transform.position = Vector3.Lerp(transform.position, TransformedVectorPosition, 0.5f);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
            if (collision.gameObject.tag == "MovableObject" && hitted && !moving)
            {
                hitedObjPosition = collision.gameObject.GetComponent<MovableObject>().ObjPosition;
                collision.gameObject.GetComponent<MovableObject>().ObjPosition = this.ObjPosition;
                collision.gameObject.GetComponent<MovableObject>().TransformedVectorPosition = collision.gameObject.GetComponent<MovableObject>().ObjPosition;
                ObjPosition = hitedObjPosition;
                moving = true;
            Vibration.Vibrate(100);
            StartCoroutine(movingActivator());
            }
            if(collision.gameObject.tag == "MainBall")
        {
            Vibration.Vibrate(100);
        }
    }

    IEnumerator movingActivator()
    {
        for(int i = 0; i < 1; i++)
        {
            yield return new WaitForSeconds(1);
        }
        moving = false;
    }
}
