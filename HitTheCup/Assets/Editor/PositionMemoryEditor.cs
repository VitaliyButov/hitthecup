﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(LevelManager))]
public class PositionMemoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Remember"))
        {
            GameObject.Find("LevelManager").GetComponent<LevelManager>().Remember();
        }
        if (GUILayout.Button("Restore"))
        {
            GameObject.Find("LevelManager").GetComponent<LevelManager>().RestoreAllObjectsOnScene();
        }
    }
}
